/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     17/08/2018 10:09:24 p.m.                     */
/*==============================================================*/


drop table AGRUPACIONES_PAISES;

drop table BARRIOS;

drop table CIUDADES;

drop table COMPANIAS;

drop table COMPANIAS_CONTACTOS;

drop table DEPARTAMENTOS;

drop table ESTADOS;

drop table LOCALIDADES;

drop table MODULOS;

drop table NACIONALIDADES;

drop table PAISES;

drop table PARAMETROS;

drop table PASS_AUD;

drop table PERFILES;

drop table PERMISOS;

drop table SUCURSALES;

drop table SUCURSALES_CONTACTOS;

drop table TIPOS_CONTACTOS;

drop table TIPOS_DIRECCIONES;

drop table TIPOS_DOCUMENTOS;

drop table USUARIOS;

/*==============================================================*/
/* Table: AGRUPACIONES_PAISES                                   */
/*==============================================================*/
create table AGRUPACIONES_PAISES (
   AGRUPACION_PAIS_ID   SERIAL not null,
   ESTADO_ID            INT4                 null,
   AGRUPACION_PAIS      VARCHAR(500)         null,
   constraint PK_AGRUPACIONES_PAISES primary key (AGRUPACION_PAIS_ID)
);

/*==============================================================*/
/* Table: BARRIOS                                               */
/*==============================================================*/
create table BARRIOS (
   BARRIO_ID            SERIAL not null,
   ESTADO_ID            INT4                 null,
   CIUDAD_ID            INT4                 null,
   BARRIO               VARCHAR(100)         null,
   constraint PK_BARRIOS primary key (BARRIO_ID)
);

/*==============================================================*/
/* Table: CIUDADES                                              */
/*==============================================================*/
create table CIUDADES (
   CIUDAD_ID            SERIAL not null,
   DEPARTAMENTO_ID      INT4                 null,
   ESTADO_ID            INT4                 null,
   CIUDAD               VARCHAR(100)         null,
   constraint PK_CIUDADES primary key (CIUDAD_ID)
);

/*==============================================================*/
/* Table: COMPANIAS                                             */
/*==============================================================*/
create table COMPANIAS (
   COMPANIA_ID          SERIAL not null,
   ESTADOID             INT4                 null,
   CIUDAD_ID            INT4                 null,
   BARRIO_ID            INT4                 null,
   NOMBRE               VARCHAR(500)         null,
   DIRECCION_CALLE1     VARCHAR(100)         null,
   DIRECCION_CALLE2     VARCHAR(100)         null,
   LEMA                 VARCHAR(1000)        null,
   DIRECCION_NRO        VARCHAR(100)         null,
   SITIO_WEB            VARCHAR(1000)        null,
   constraint PK_COMPANIAS primary key (COMPANIA_ID)
);

/*==============================================================*/
/* Table: COMPANIAS_CONTACTOS                                   */
/*==============================================================*/
create table COMPANIAS_CONTACTOS (
   COMPANIA_CONTACTO_ID SERIAL not null,
   TIPO_CONTACTO_ID     INT4                 null,
   ESTADO_ID            INT4                 null,
   COMPANIA_ID          INT4                 null,
   CONTACTO             VARCHAR(100)         null,
   constraint PK_COMPANIAS_CONTACTOS primary key (COMPANIA_CONTACTO_ID)
);

/*==============================================================*/
/* Table: DEPARTAMENTOS                                         */
/*==============================================================*/
create table DEPARTAMENTOS (
   DEPARTAMENTO_ID      SERIAL not null,
   ESTADO_ID            INT4                 null,
   PAIS_ID              INT4                 null,
   DEPARTAMENTO         VARCHAR(100)         null,
   constraint PK_DEPARTAMENTOS primary key (DEPARTAMENTO_ID)
);

/*==============================================================*/
/* Table: ESTADOS                                               */
/*==============================================================*/
create table ESTADOS (
   ESTADO_ID            SERIAL not null,
   USUARIO_ID           INT4                 null,
   ESTADO               VARCHAR(100)         null,
   constraint PK_ESTADOS primary key (ESTADO_ID)
);

/*==============================================================*/
/* Table: LOCALIDADES                                           */
/*==============================================================*/
create table LOCALIDADES (
   LOCALIDAD_ID         SERIAL not null,
   ESTADO_ID            INT4                 null,
   BARRIO_ID            INT4                 null,
   LACALIDAD            VARCHAR(100)         null,
   constraint PK_LOCALIDADES primary key (LOCALIDAD_ID)
);

/*==============================================================*/
/* Table: MODULOS                                               */
/*==============================================================*/
create table MODULOS (
   MODULO_ID            SERIAL not null,
   ESTADO_ID            INT4                 null,
   MODULO               VARCHAR(500)         null,
   URL                  VARCHAR(500)         null,
   constraint PK_MODULOS primary key (MODULO_ID)
);

/*==============================================================*/
/* Table: NACIONALIDADES                                        */
/*==============================================================*/
create table NACIONALIDADES (
   NACIONALIDAD_ID      SERIAL not null,
   ESTADO_ID            INT4                 null,
   PAIS_ID              INT4                 null,
   NACIONALIDAD         VARCHAR(100)         null,
   constraint PK_NACIONALIDADES primary key (NACIONALIDAD_ID)
);

/*==============================================================*/
/* Table: PAISES                                                */
/*==============================================================*/
create table PAISES (
   PAIS_ID              SERIAL not null,
   AGRUPACION_PAIS_ID   INT4                 null,
   ESTADO_ID            INT4                 null,
   PAIS                 VARCHAR(100)         null,
   CODISO               VARCHAR(10)          null,
   CODPAIS              VARCHAR(10)          null,
   constraint PK_PAISES primary key (PAIS_ID)
);

/*==============================================================*/
/* Table: PARAMETROS                                            */
/*==============================================================*/
create table PARAMETROS (
   PARAMETRO_ID         SERIAL not null,
   ESTADO_ID            INT4                 null,
   COMPANIA_ID          INT4                 null,
   PARAMETRO            VARCHAR(500)         null,
   VALOR                VARCHAR(500)         null,
   OBSERVACION          VARCHAR(500)         null,
   constraint PK_PARAMETROS primary key (PARAMETRO_ID)
);

/*==============================================================*/
/* Table: PASS_AUD                                              */
/*==============================================================*/
create table PASS_AUD (
   PASS_AUD_ID          SERIAL not null,
   USUARIO_ID           INT4                 null,
   PASSWORD             VARCHAR(100)         null,
   FECHA_ALTA           DATE                 null,
   constraint PK_PASS_AUD primary key (PASS_AUD_ID)
);

/*==============================================================*/
/* Table: PERFILES                                              */
/*==============================================================*/
create table PERFILES (
   PERFIL_ID            SERIAL not null,
   ESTADO_ID            INT4                 null,
   COMPANIA_ID          INT4                 null,
   PERFIL               VARCHAR(500)         null,
   constraint PK_PERFILES primary key (PERFIL_ID)
);

/*==============================================================*/
/* Table: PERMISOS                                              */
/*==============================================================*/
create table PERMISOS (
   PERMISO_ID           SERIAL not null,
   MODULO_ID            INT4                 null,
   PERFIL_ID            INT4                 null,
   ESTADO_ID            INT4                 null,
   CONSULTA             INT4                 null,
   ALTA                 INT4                 null,
   BAJA                 INT4                 null,
   MODIFICACION         INT4                 null,
   DESBLOQUEAR          INT4                 null,
   constraint PK_PERMISOS primary key (PERMISO_ID)
);

/*==============================================================*/
/* Table: SUCURSALES                                            */
/*==============================================================*/
create table SUCURSALES (
   SUCURSAL_ID          SERIAL not null,
   ESTADO_ID            INT4                 null,
   COMPANIA_ID          INT4                 null,
   CIUDAD_ID            INT4                 null,
   BARRIO_ID            INT4                 null,
   SUCURSAL             VARCHAR(500)         null,
   DIRECCION            VARCHAR(500)         null,
   LATITUD              VARCHAR(500)         null,
   LONGITUD             VARCHAR(500)         null,
   constraint PK_SUCURSALES primary key (SUCURSAL_ID)
);

/*==============================================================*/
/* Table: SUCURSALES_CONTACTOS                                  */
/*==============================================================*/
create table SUCURSALES_CONTACTOS (
   SUCURSAL_CONTACTO_ID SERIAL not null,
   ESTADO_ID            INT4                 null,
   SUCURSAL_ID          INT4                 null,
   TIPO_CONTACTO_ID     INT4                 null,
   CONTACTO             VARCHAR(500)         null,
   constraint PK_SUCURSALES_CONTACTOS primary key (SUCURSAL_CONTACTO_ID)
);

/*==============================================================*/
/* Table: TIPOS_CONTACTOS                                       */
/*==============================================================*/
create table TIPOS_CONTACTOS (
   TIPO_CONTACTO_ID     SERIAL not null,
   ESTADO_ID            INT4                 null,
   TIPO_CONTACTO        VARCHAR(100)         null,
   constraint PK_TIPOS_CONTACTOS primary key (TIPO_CONTACTO_ID)
);

/*==============================================================*/
/* Table: TIPOS_DIRECCIONES                                     */
/*==============================================================*/
create table TIPOS_DIRECCIONES (
   TIPO_DIRECCION_ID    SERIAL not null,
   ESTADO_ID            INT4                 null,
   TIPO_DIRECCION       VARCHAR(100)         null,
   constraint PK_TIPOS_DIRECCIONES primary key (TIPO_DIRECCION_ID)
);

/*==============================================================*/
/* Table: TIPOS_DOCUMENTOS                                      */
/*==============================================================*/
create table TIPOS_DOCUMENTOS (
   TIPO_DOCUMENTO_ID    SERIAL not null,
   ESTADO_ID            INT4                 null,
   TIPO_DOCUMENTO       VARCHAR(100)         null,
   constraint PK_TIPOS_DOCUMENTOS primary key (TIPO_DOCUMENTO_ID)
);

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
create table USUARIOS (
   USUARIO_ID           SERIAL not null,
   ESTADO_ID            INT4                 null,
   PERFIL_ID            INT4                 null,
   COMPANIA_ID          INT4                 null,
   NICK                 VARCHAR(100)         null,
   PASSWORD             VARCHAR(500)         null,
   ADMIN                INT4                 null,
   PRIMERA_VEZ          INT4                 null,
   MODIFICAR            INT4                 null,
   _FECHA_ALTA__        DATE                 null,
   FECHA_MOD            DATE                 null,
   LAST_LOGIN           DATE                 null,
   LAST_IP              VARCHAR(100)         null,
   LAST_BROWSER         VARCHAR(100)         null,
   EMAIL                VARCHAR(100)         null,
   constraint PK_USUARIOS primary key (USUARIO_ID)
);

alter table AGRUPACIONES_PAISES
   add constraint FK_AGRUPACI_FK_AGRUPA_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table BARRIOS
   add constraint FK_BARRIOS_FK_BARRIO_CIUDADES foreign key (CIUDAD_ID)
      references CIUDADES (CIUDAD_ID)
      on delete restrict on update restrict;

alter table BARRIOS
   add constraint FK_BARRIOS_FK_BARRIO_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table CIUDADES
   add constraint FK_CIUDADES_FK_CIUDAD_DEPARTAM foreign key (DEPARTAMENTO_ID)
      references DEPARTAMENTOS (DEPARTAMENTO_ID)
      on delete restrict on update restrict;

alter table CIUDADES
   add constraint FK_CIUDADES_FK_CIUDAD_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table COMPANIAS
   add constraint FK_COMPANIA_FK_COMPAN_ESTADOS foreign key (ESTADOID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table COMPANIAS
   add constraint FK_COMPANIA_FK_COMPAN_BARRIOS foreign key (BARRIO_ID)
      references BARRIOS (BARRIO_ID)
      on delete restrict on update restrict;

alter table COMPANIAS
   add constraint FK_COMPANIA_FK_COMPAN_CIUDADES foreign key (CIUDAD_ID)
      references CIUDADES (CIUDAD_ID)
      on delete restrict on update restrict;

alter table COMPANIAS_CONTACTOS
   add constraint FK_COMPANIA_FK_COMPAN_TIPOS_CO foreign key (TIPO_CONTACTO_ID)
      references TIPOS_CONTACTOS (TIPO_CONTACTO_ID)
      on delete restrict on update restrict;

alter table COMPANIAS_CONTACTOS
   add constraint FK_COMPANIA_FK_COMPAN_COMPANIA foreign key (COMPANIA_ID)
      references COMPANIAS (COMPANIA_ID)
      on delete restrict on update restrict;

alter table COMPANIAS_CONTACTOS
   add constraint FK_COMPANIA_FK_COMPAN_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table DEPARTAMENTOS
   add constraint FK_DEPARTAM_FK_DEPART_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table DEPARTAMENTOS
   add constraint FK_DEPARTAM_FK_DEPART_PAISES foreign key (PAIS_ID)
      references PAISES (PAIS_ID)
      on delete restrict on update restrict;

alter table ESTADOS
   add constraint FK_ESTADOS_REFERENCE_USUARIOS foreign key (USUARIO_ID)
      references USUARIOS (USUARIO_ID)
      on delete restrict on update restrict;

alter table LOCALIDADES
   add constraint FK_LOCALIDA_FK_LOCALI_BARRIOS foreign key (BARRIO_ID)
      references BARRIOS (BARRIO_ID)
      on delete restrict on update restrict;

alter table LOCALIDADES
   add constraint FK_LOCALIDA_FK_LOCALI_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table MODULOS
   add constraint FK_MODULOS_FK_MODULO_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table NACIONALIDADES
   add constraint FK_NACIONAL_FK_NACION_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table NACIONALIDADES
   add constraint FK_NACIONAL_FK_NACION_PAISES foreign key (PAIS_ID)
      references PAISES (PAIS_ID)
      on delete restrict on update restrict;

alter table PAISES
   add constraint FK_PAISES_FK_PAIS_A_AGRUPACI foreign key (AGRUPACION_PAIS_ID)
      references AGRUPACIONES_PAISES (AGRUPACION_PAIS_ID)
      on delete restrict on update restrict;

alter table PAISES
   add constraint FK_PAISES_FK_PAIS_E_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table PARAMETROS
   add constraint FK_PARAMETR_FK_PARAME_COMPANIA foreign key (COMPANIA_ID)
      references COMPANIAS (COMPANIA_ID)
      on delete restrict on update restrict;

alter table PARAMETROS
   add constraint FK_PARAMETR_FK_PARAME_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table PASS_AUD
   add constraint FK_PASS_AUD_FK_PASS_A_USUARIOS foreign key (USUARIO_ID)
      references USUARIOS (USUARIO_ID)
      on delete restrict on update restrict;

alter table PERFILES
   add constraint FK_PERFILES_FK_PERFIL_COMPANIA foreign key (COMPANIA_ID)
      references COMPANIAS (COMPANIA_ID)
      on delete restrict on update restrict;

alter table PERFILES
   add constraint FK_PERFILES_FK_PERFIL_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table PERMISOS
   add constraint FK_PERMISOS_FK_PERMIS_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table PERMISOS
   add constraint FK_PERMISOS_FK_PERMIS_MODULOS foreign key (MODULO_ID)
      references MODULOS (MODULO_ID)
      on delete restrict on update restrict;

alter table PERMISOS
   add constraint FK_PERMISOS_FK_PERMIS_PERFILES foreign key (PERFIL_ID)
      references PERFILES (PERFIL_ID)
      on delete restrict on update restrict;

alter table SUCURSALES
   add constraint FK_SUCURSAL_FK_SUCURS_BARRIOS foreign key (BARRIO_ID)
      references BARRIOS (BARRIO_ID)
      on delete restrict on update restrict;

alter table SUCURSALES
   add constraint FK_SUCURSAL_FK_SUCURS_CIUDADES foreign key (CIUDAD_ID)
      references CIUDADES (CIUDAD_ID)
      on delete restrict on update restrict;

alter table SUCURSALES
   add constraint FK_SUCURSAL_FK_SUCURS_COMPANIA foreign key (COMPANIA_ID)
      references COMPANIAS (COMPANIA_ID)
      on delete restrict on update restrict;

alter table SUCURSALES
   add constraint FK_SUCURSAL_FK_SUCURS_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table SUCURSALES_CONTACTOS
   add constraint FK_SUCURSAL_FK_SUCURS_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table SUCURSALES_CONTACTOS
   add constraint FK_SUCURSAL_FK_SUCURS_SUCURSAL foreign key (SUCURSAL_ID)
      references SUCURSALES (SUCURSAL_ID)
      on delete restrict on update restrict;

alter table SUCURSALES_CONTACTOS
   add constraint FK_SUCURSAL_FK_SUCURS_TIPOS_CO foreign key (TIPO_CONTACTO_ID)
      references TIPOS_CONTACTOS (TIPO_CONTACTO_ID)
      on delete restrict on update restrict;

alter table TIPOS_CONTACTOS
   add constraint FK_TIPOS_CO_FK_TIPO_C_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table TIPOS_DIRECCIONES
   add constraint FK_TIPOS_DI_FK_TIPO_D_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table TIPOS_DOCUMENTOS
   add constraint FK_TIPOS_DO_FK_TIPO_D_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table USUARIOS
   add constraint FK_USUARIOS_FK_USUARI_COMPANIA foreign key (COMPANIA_ID)
      references COMPANIAS (COMPANIA_ID)
      on delete restrict on update restrict;

alter table USUARIOS
   add constraint FK_USUARIOS_FK_USUARI_ESTADOS foreign key (ESTADO_ID)
      references ESTADOS (ESTADO_ID)
      on delete restrict on update restrict;

alter table USUARIOS
   add constraint FK_USUARIOS_FK_USUARI_PERFILES foreign key (PERFIL_ID)
      references PERFILES (PERFIL_ID)
      on delete restrict on update restrict;

